NC A&T Comp410 Fall 2019 Project
-
This project is conducted by the students in Comp410 at NC A&T during Fall 2019 in partnership with Cisco Systems. 

The intention of the project is to leverage the Webex Teams API in order to develop a web application that displays interesting metrics and insights for any given Webex Teams Room/Team.

## Contacts
The primary contact method between students and Cisco for this class will be through Webex Teams. All students and instructors should [download](https://www.webex.com/downloads.html) the Webex Teams app and ensure they have been added to the [*EXTERNAL COMP410 Fall 2019*](https://teams.webex.com/teams/68841d00-c8e7-11e9-9dff-d3d75ba6bd92) Webex Team. You may need to register an account with Webex, which should be done with your University Email address. If you do not have access to the team mentioned, please reach out to one of the representatives from Cisco below who are helping run this project.
1. Mike Claes - mclaes@cisco.com
2. Cheick Berthe - cberthe@cisco.com
3. David Meyer - dameyer2@cisco.com

## Environment Setup

#### Python

This project will use Python 3.5+ so please check that you have a version within this range and you aren't using 2.7.

#### PyCharm

We will be using the PyCharm IDE as our primary development environment. If you create your Jetbrains account with your University email, you should be able to [download](https://www.jetbrains.com/pycharm/download/) the Professional edition at no charge, however the Community edition will work just fine for this course as well.

##### Project Interpreter

In PyCharm, you have the ability to create a Python virtual environment specifically tailored to this projects requirements. Please create a [new Project Interpreter](https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html#add_new_project_interpreter) for this project.

Once the new Project Interpreter is created, please [configure](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html#Installing_Uninstalling_and_Upgrading_Packages.xml) this environment to include the python packages listed in the [requirements.txt](requirements.txt) file.

##### Run Configuration

PyCharm also allows you to save the configuration parameters for how a particular Python file/script should run. It is recommended to [create a new Run Configuration](https://www.jetbrains.com/help/pycharm/run-debug-configuration-python.html) for each file that you intend to execute. Pay close attention to the environment variables section which will need to be modified to include the default environment variable (WEBEX_TEAMS_ACCESS_TOKEN) and [credentials](https://webexteamssdk.readthedocs.io/en/latest/user/quickstart.html#use-your-webex-teams-access-token) required to access the Webex Teams API.

#### Webex Teams API

We will be using the PyPi package [webexteamssdk](https://webexteamssdk.readthedocs.io/en/latest/) to interact with the Webex Teams REST API. To authenticate to the API, follow the instructions below.

1. Browse to https://developer.webex.com/
2. Login with your webex credentials
3. Click Goto Docs
4. Click on Getting Started
5. Scroll down to find your Personal Access Token and copy it to your clipboard
6. Create an environment variable WEBEX_TEAMS_ACCESS_TOKEN and set to your access token

## Sprint 1 Assignment

[Sample Code](sample/sprint1/make_table.py)



