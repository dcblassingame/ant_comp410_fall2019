# Important how-to use this module information
#
# The webexteamssdk uses a token which must be set in your
# environment to authenticate to webex teams.
#
# (1) Browse to https://developer.webex.com/
# (2) Login with your webex credentials
# (3) Click Goto Docs
# (4) Click on Getting Started
# (5) Scroll down to find your Personal Access Token
# (6) Create an environment variable WEBEX_TEAMS_ACCESS_TOKEN and set to your access token
# (7) Run this script, result will be a data table in csv format

import os
import pprint
import sys

import pandas as pd
from webexteamssdk import WebexTeamsAPI

# define a pretty-printer for diagnostics
pp = pprint.PrettyPrinter(indent=4)


# Check to make sure environment variable is set
def check_environment():
    try:
        os.environ['WEBEX_TEAMS_ACCESS_TOKEN']
    except KeyError:
        print('Please set WEBEX_TEAMS_ACCESS_TOKEN environment variable')
        print('You can retrieve a token here:')
        print('https://developer.webex.com/docs/api/getting-started')
        sys.exit(1)


# Returns a dataframe which lists the members of a webex team
# and all possible details about them.
#
# Associated access token must be a member of the requested team
def get_team_member_info_as_df(team_name: str):
    # Create a WebexTeamsAPI connection object
    # uses your WEBEX_TEAMS_ACCESS_TOKEN - make sure this is set
    # For more info on webexteamssdk check out https://github.com/CiscoDevNet/webexteamssdk
    wbxapi = WebexTeamsAPI()

    # Get list of teams
    my_teams = wbxapi.teams.list()

    # Search for requested team
    team_id = None
    for t in my_teams:
        if t.name == team_name:
            team_id = t.id
    if team_id is None:
        print('Team was not found - ' + team_name)
        return None

    # Make a df to hold member info
    df = pd.DataFrame()
    print('Creating dataframe for ' + team_name)

    # Get all members of this team
    members = wbxapi.team_memberships.list(teamId=team_id)

    for m in members:
        # Get additional info about this person
        person_info = wbxapi.people.get(m.personId)

        # Make this a dict and add the lastActivity
        member_dict = m.to_dict()
        person_dict = person_info.to_dict()

        # Null out lastActivity if not present
        if 'lastActivity' not in person_dict:
            member_dict['lastActivity'] = ''
        else:
            member_dict['lastActivity'] = person_dict['lastActivity']

        # Remove redundant id column
        del member_dict['id']

        # print diagnostics
        print('---' + member_dict['personEmail'] + '---')
        pp.pprint(member_dict)

        # Add this member to the df
        df = df.append(member_dict, ignore_index=True)

    # Convert isModerator to int
    # 1 = person is a moderator
    # 0 = person is not a bode
    df['isModerator'] = df['isModerator'].astype('int')

    # Return complete dataframe of all members with their info
    return df


def make_table():
    # quick check to make sure env looks ok
    check_environment()

    # get members of a webex team
    df = get_team_member_info_as_df('EXTERNAL COMP410 Fall 2019')

    # make sure return was valid before writing data table in csv format
    if df is not None:
        df.to_csv('team_members.csv', index=False)


if __name__ == '__main__':
    make_table()
